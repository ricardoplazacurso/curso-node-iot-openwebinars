'use strict'

const app = require('./app')

if (process.env.nodeEnv !== 'PRO')  var config = require('./config-test')
else var config = require('./config')

var express = require('express')();

var Raven = require('raven');
Raven.config(config.sentry).install();

// The request handler must be the first middleware on the app
app.use(Raven.requestHandler());

// The error handler must be before any other error middleware
app.use(Raven.errorHandler());


app.listen(config.port, () => {

    console.log ("API running on port: " + config.port);

})
