'use strict'

const express = require('express')
const ops = require('../controllers/ops')
const test = require('../controllers/test')
const api = express.Router()

//OPS

api.get('/health', ops.health)

// TEST

api.get('/test', test.test)


module.exports = api
