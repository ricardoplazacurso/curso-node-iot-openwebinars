'use strict'

module.exports = {
    port: process.env.PORT || 8000,
    sentry: process.env.sentryDsn,
    release: "curso-node-iot-openwebinars@0.0.0"
}
